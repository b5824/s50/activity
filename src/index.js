import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/* 
  Sample component
*/
// const name = 'John Doe';
// const user = {
//   firstName: 'Jane',
//   lastName: 'Doe',
// };

// const formatName = (user) => {
//   return `${user.firstName} ${user.lastName}`;
// };

// const element = <h1>Hello, {formatName(user)}</h1>;

/* 
So its function is to extract the section from the root file (index.html) we want to apply edits to, in this case the div section which has the id root? 
*/
//What do you want to show
// const root = ReactDOM.createRoot(document.getElementById('root'));
//Where do you want to show it
// root.render(element);
