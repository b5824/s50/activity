import { Row, Col, Card, Button } from 'react-bootstrap';

export default function CourseCard() {
  return (
    <Row className='mt-3 mb-3'>
      <Col xs={12} md={12}>
        <Card className='cardHighlight p-3'>
          <Card.Body>
            <Card.Title>
              <h2>Sample Course</h2>
            </Card.Title>
            <Card.Text>
              <h6>Description</h6>
              <p>This is a sample offering</p>
            </Card.Text>
            <Card.Text>Price:</Card.Text>
            <Card.Text>PhP 40,000</Card.Text>
            <Button xs={3} md={3} variant='primary'>
              Enroll
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
