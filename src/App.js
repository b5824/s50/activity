import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import './App.css';
import Home from './Pages/Home';

function App() {
  return (
    <div>
      <AppNavBar />
      <Container>
        <Home />
      </Container>
    </div>
  );
}

export default App;
